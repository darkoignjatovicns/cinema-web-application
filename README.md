# Cinema 

Web Application (Group project)

### Prerequisites

XAMPP - Apache 

XAMPP - MySQL Server 

PhpStorm, Visual Code ...

MySQL Workbench, PhpMyAdmin ...

### Installing

1. Deploy project on server

2. Change project name in "project"

3. Import sql script in PhpMyAdmin

4. Change db_config.php file

5. type in url: http://localhost:80/project

## Deployment

Clone this project in htdocs folder, or move project folder in htdocs folder

## Author

**Darko Ignjatović** and more two contributors




